package top.blackcat.learn.shiro.r;


import cn.hutool.core.date.DateTime;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import top.blackcat.learn.shiro.enums.ResultCodeEnum;

@ApiModel(description= "返回响应数据")
@Data
public class R<T> {

    @ApiModelProperty(value = "响应时间")
    private String  timestamp = DateTime.now().toString("yyyy-MM-dd HH:mm:ss");
    @ApiModelProperty(value = "是否成功")
    private Boolean success;
    @ApiModelProperty(value = "错误编号")
    private Integer code;
    @ApiModelProperty(value = "错误信息")
    private String msg;

    /***
     * 返回数据
     */
    private T data;

    /***
     * 构造器私有
     */
    private R() {
    }

    /***
     * 通用返回成功
     * @return
     */
    public static R ok() {
        R r = new R();
        r.setSuccess(ResultCodeEnum.SUCCESS.getSuccess());
        r.setCode(ResultCodeEnum.SUCCESS.getCode());
        r.setMsg(ResultCodeEnum.SUCCESS.getMessage());
        return r;
    }

    /***
     * 通用返回失败
     * @return
     */
    public static R error() {
        R r = new R();
        r.setSuccess(ResultCodeEnum.UNKNOWN_ERROR.getSuccess());
        r.setCode(ResultCodeEnum.UNKNOWN_ERROR.getCode());
        r.setMsg(ResultCodeEnum.UNKNOWN_ERROR.getMessage());
        // 通用返回失败，未知错误
        return r;
    }

    /***
     * 设置结果，形参为结果枚举
     * @param result
     * @return
     */
    public static R setResult(ResultCodeEnum result) {
        R r = new R();
        r.setSuccess(result.getSuccess());
        r.setCode(result.getCode());
        r.setMsg(result.getMessage());
        return r;
    }

    /**
     * ------------使用链式编程，返回类本身-----------
     **/

    /***
     * 自定义返回数据
     * @param data
     * @return
     */
    public R data(T data) {
        this.setData(data);
        return this;
    }


    /***
     *  自定义状态信息
     * @param message
     * @return
     */
    public R message(String message) {
        this.setMsg(message);
        return this;
    }
    /***
     *  自定义状态码
     * @param code
     * @return
     */
    public R code(Integer code) {
        this.setCode(code);
        return this;
    }

    /***
     *  自定义返回结果
     * @param success
     * @return
     */
    public R success(Boolean success) {
        this.setSuccess(success);
        return this;
    }
}
