package top.blackcat.learn.shiro.enums;

public enum RedisKeyEnum {

    USER_TOKEN("token:%s"),
    ZHI_HU_TOP("zhihu_top"),
    DOU_YIN_TOP("douyin_top"),
    WEI_BO_TOP("weibo_top");

    private String msg;

    RedisKeyEnum(String msg) {
        this.msg = msg;
    }


    public String getMsg() {
        return msg;
    }
}
