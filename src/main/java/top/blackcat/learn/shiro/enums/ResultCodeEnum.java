package top.blackcat.learn.shiro.enums;


import lombok.Getter;


@Getter
public enum ResultCodeEnum {

    SUCCESS(true, 200, "成功"),
    PARAM_ERROR(false, 400, "参数错误"),
    UNKNOWN_ERROR(false, 500, "服务器错误"),
    ;

    /***
     * 响应是否成功
     */
    private Boolean success;
    /***
     * 响应状态码
     */
    private Integer code;
    /***
     * 响应信息
     */
    private String message;

    ResultCodeEnum(boolean success, Integer code, String message) {
        this.success = success;
        this.code = code;
        this.message = message;
    }
}