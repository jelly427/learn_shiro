package top.blackcat.learn.shiro;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import springfox.documentation.oas.annotations.EnableOpenApi;

@EnableOpenApi
@SpringBootApplication
@ServletComponentScan
@MapperScan("top.blackcat.learn.shiro.mapper")
public class LearnShiroApplication {

	public static void main(String[] args) {
		SpringApplication.run(LearnShiroApplication.class, args);
	}

}
