package top.blackcat.learn.shiro.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class SysPermission implements Serializable {

    private String id;
    private String permission;
    private String roleId;
    private Date createTime;

}