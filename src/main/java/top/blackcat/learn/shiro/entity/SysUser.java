package top.blackcat.learn.shiro.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class SysUser implements Serializable {

    private String id;
    private String username;
    private String password;
    private String roleId;
    private Date createTime;
}