package top.blackcat.learn.shiro.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SysRole implements Serializable {

    private String   id;
    private String roleName;
    private String userId;
    private String permissionsId;
    private Date   createTime;

}