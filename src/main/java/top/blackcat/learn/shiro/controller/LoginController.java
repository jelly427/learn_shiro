package top.blackcat.learn.shiro.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.ShiroException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.apache.shiro.crypto.hash.Md5Hash;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import top.blackcat.learn.shiro.exception.ExceptionRes.ApiError;
import top.blackcat.learn.shiro.r.R;
import top.blackcat.learn.shiro.utils.JWTUtils;
import top.blackcat.learn.shiro.exception.ServiceException;
import top.blackcat.learn.shiro.service.LoginService;


@Api(tags = "登陆")
@RestController
@RequestMapping("/")
public class LoginController {

    @Autowired
    private LoginService loginService;

    @Autowired
    private JWTUtils jwtUtils;

    @ApiOperation(value = "添加用户")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "username", value = "用户名", paramType = "query", required = true),
            @ApiImplicitParam(name = "password", value = "密码", paramType = "query", required = true),
    })
    @PostMapping("/addUser")
    public R addUser(String username, String password) throws ServiceException {
        Md5Hash md5Hash = new Md5Hash(password, username, 1024);
        String msg = loginService.addUser(username, md5Hash.toHex());
        return R.ok().data(msg);
    }

    /**
     * POST登录
     *
     * @param username
     * @param password
     * @return
     */
    @ApiOperation(value = "用户登陆")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "username", value = "用户名", paramType = "query", required = true),
            @ApiImplicitParam(name = "password", value = "密码", paramType = "query", required = true)
    })
    @PostMapping("/login")
    public R login(String username, String password) {
        Md5Hash md5Hash = new Md5Hash(password, username, 1024);
        // 添加用户认证信息
        UsernamePasswordToken usernamePasswordToken = new UsernamePasswordToken(username, md5Hash.toHex());
        // 进行验证，这里可以捕获异常，然后返回对应信息
        Subject subject = SecurityUtils.getSubject();
        try {
            subject.login(usernamePasswordToken);
            String token = jwtUtils.createToken(username);
            return R.ok().data(token);
        } catch (ShiroException exception) {
            return R.error().message(ApiError.ERROR_40000003.getMsg()).data(exception.getMessage());
        }
    }


    /**
     * 注解的使用
     *
     * @return
     */
    @RequiresRoles("admin")
    @RequiresPermissions("create")
    @GetMapping(value = "/create")
    public String create() {
        return "Create success!";
    }


    @GetMapping(value = "/index")
    public String index() {
        return "This is index";
    }


    @GetMapping(value = "/error")
    public String error() {
        return "Auth error,please login.";
    }

    /**
     * 退出的时候是get请求，主要是用于退出
     *
     * @return
     */
    @GetMapping(value = "/logout")
    public String logout() {
        return "logout";
    }


}


