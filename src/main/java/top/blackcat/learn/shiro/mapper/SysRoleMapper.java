package top.blackcat.learn.shiro.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import top.blackcat.learn.shiro.entity.SysRole;

@Mapper
public interface SysRoleMapper extends BaseMapper<SysRole> {

}
