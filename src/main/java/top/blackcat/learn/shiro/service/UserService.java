package top.blackcat.learn.shiro.service;

import top.blackcat.learn.shiro.entity.SysPermission;
import top.blackcat.learn.shiro.entity.SysRole;
import top.blackcat.learn.shiro.entity.SysUser;

import java.util.List;

public interface UserService {

    List<SysRole> getUserRoles(String id);

    List<SysPermission> getUserPermissions(String id);

    List<SysUser> getUserByName(String username);

    List<String> getPrivListByName(String username);

}
