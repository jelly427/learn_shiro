package top.blackcat.learn.shiro.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import top.blackcat.learn.shiro.entity.SysPermission;
import top.blackcat.learn.shiro.entity.SysRole;
import top.blackcat.learn.shiro.entity.SysUser;
import top.blackcat.learn.shiro.mapper.SysPermissionMapper;
import top.blackcat.learn.shiro.mapper.SysRoleMapper;
import top.blackcat.learn.shiro.mapper.SysUserMapper;
import top.blackcat.learn.shiro.service.UserService;

import java.util.List;


@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private SysRoleMapper sysRoleMapper;

    @Autowired
    private SysPermissionMapper sysPermissionMapper;

    @Override
    public List<SysUser> getUserByName(String username) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("username", username);
        queryWrapper.orderByDesc("create_time");
        return sysUserMapper.selectList(queryWrapper);
    }

    @Override
    public List<String> getPrivListByName(String username) {
        return null;
    }

    @Override
    public List<SysRole> getUserRoles(String userId) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("user_id", userId);
        return sysRoleMapper.selectList(queryWrapper);
    }

    @Override
    public List<SysPermission> getUserPermissions(String roleId) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("role_id", roleId);
        return sysPermissionMapper.selectList(queryWrapper);
    }


}
