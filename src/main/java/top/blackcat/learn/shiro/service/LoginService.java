package top.blackcat.learn.shiro.service;


import top.blackcat.learn.shiro.entity.SysUser;
import top.blackcat.learn.shiro.exception.ServiceException;

public interface LoginService {

    SysUser findByName(String name) throws ServiceException;

    String addUser(String username, String password) throws ServiceException;

}
