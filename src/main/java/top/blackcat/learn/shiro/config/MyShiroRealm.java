package top.blackcat.learn.shiro.config;

import lombok.SneakyThrows;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import top.blackcat.learn.shiro.entity.SysPermission;
import top.blackcat.learn.shiro.entity.SysRole;
import top.blackcat.learn.shiro.entity.SysUser;
import top.blackcat.learn.shiro.service.UserService;

import java.util.List;

/**
 * 实现AuthorizingRealm
 * 接口用户用户认证
 * @author Louis
 * @date Jun 20, 2019
 */
public class MyShiroRealm extends AuthorizingRealm {

    @Autowired
    private UserService userService;

    /**
     * 用户认证
     */
    @SneakyThrows
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken)
            throws AuthenticationException {
        // 加这一步的目的是在Post请求的时候会先进认证，然后在到请求
        if (authenticationToken.getPrincipal() == null) {
            return null;
        }
        // 获取用户信息
        String name = authenticationToken.getPrincipal().toString();
        List<SysUser> users = userService.getUserByName(name);
        if (CollectionUtils.isEmpty(users)) {
            // 这里返回后会报出对应异常
            return null;
        } else {
            SysUser user = users.get(0);
            // 这里验证authenticationToken和simpleAuthenticationInfo的信息
            SimpleAuthenticationInfo simpleAuthenticationInfo = new SimpleAuthenticationInfo(name,
                    user.getPassword(), getName());
            return simpleAuthenticationInfo;
        }
    }

    /**
     * 角色权限和对应权限添加
     */
    @SneakyThrows
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        // 获取登录用户名
        String name = (String) principalCollection.getPrimaryPrincipal();
        // 查询用户名称
        List<SysUser> users = userService.getUserByName(name);
        if (CollectionUtils.isEmpty(users)) {
            // 这里返回后会报出对应异常
            return null;
        }
        SysUser user = users.get(0);
        // 添加角色和权限
        SimpleAuthorizationInfo simpleAuthorizationInfo = new SimpleAuthorizationInfo();
        List<SysRole> roles = userService.getUserRoles(user.getId());
        for (SysRole role : roles) {
            // 添加角色
            simpleAuthorizationInfo.addRole(role.getRoleName());
            List<SysPermission> permissions = userService.getUserPermissions(role.getId());
            for (SysPermission permission : permissions) {
                // 添加权限
                simpleAuthorizationInfo.addStringPermission(permission.getPermission());
            }
        }
        return simpleAuthorizationInfo;
    }

}