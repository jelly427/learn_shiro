package top.blackcat.learn.shiro.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;
import top.blackcat.learn.shiro.handler.JWTHandler;


/**
 * @author HaiBo
 * @createDate 2021/8/4 10:01
 * @version: 1.0
 * @desc web mvc 配置类
 **/
@Configuration
public class MyHandlerConfig extends WebMvcConfigurationSupport {

    /*** client request handler ***/
    @Autowired
    private JWTHandler jwtHandler;

    @Override
    protected void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(jwtHandler)
                 //  api
                .addPathPatterns("/api/*")
                 // error
                .excludePathPatterns("/error")
                 // login api
                .excludePathPatterns("/login");
    }

}
