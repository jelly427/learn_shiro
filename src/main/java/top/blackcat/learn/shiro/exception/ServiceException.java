package top.blackcat.learn.shiro.exception;


import lombok.Data;
import top.blackcat.learn.shiro.exception.ExceptionRes.ApiError;
import top.blackcat.learn.shiro.exception.ExceptionRes.ApiRest;

@Data
public class ServiceException extends Exception {

    /**
     * 错误码
     */
    private Integer code;

    /**
     * 错误消息
     */
    private String msg;

    /**
     * 从结果初始化
     *
     * @param apiRest
     */
    public ServiceException(ApiRest apiRest) {
        this.code = apiRest.getCode();
        this.msg = apiRest.getMsg();
    }

    /**
     * 从枚举中获取参数
     *
     * @param apiError
     */
    public ServiceException(ApiError apiError) {
        this.code = apiError.getCode();
        this.msg = apiError.msg;
    }

}
