package top.blackcat.learn.shiro.exception.ExceptionRes;


import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 全局错误码定义，用于定义接口的响应数据，
 * 枚举名称全部使用代码命名，在系统中调用，免去取名难的问题。
 *
 * @author bool
 * @date 2019-06-14 21:15
 */
@NoArgsConstructor
@AllArgsConstructor
public enum ApiError implements Serializable {

    /**
     * 通用错误，接口参数不全
     */
    ERROR_10010001("参数不全或类型错误！"),
    ERROR_10010002("您还未登录，请先登录！"),
    ERROR_10010003("不允许重复用户名！"),
    ERROR_40000000("数据不存在！"),
    ERROR_40000001("token已过期！"),
    ERROR_40000002("token校验不通过！"),
    ERROR_40000003(" 用户名/密码错误！"),
    ERROR_90000000("数据不存在！");


    public String msg;


    /**
     * 生成Markdown格式文档，用于更新文档用的
     *
     * @param args
     */
    public static void main(String[] args) {
        for (ApiError e : ApiError.values()) {
            System.out.println("'" + e.name().replace("ERROR_", "") + "':'" + e.msg + "',");
        }
    }

    /**
     * 获取错误码
     *
     * @return
     */
    public Integer getCode() {
        return Integer.parseInt(this.name().replace("ERROR_", ""));
    }

    public String getMsg() {
        return msg;
    }


}
