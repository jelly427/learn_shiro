package top.blackcat.learn.shiro.exception.ExceptionRes;


import cn.hutool.core.date.DateTime;
import lombok.Data;
import lombok.NoArgsConstructor;
import top.blackcat.learn.shiro.exception.ServiceException;

@Data
@NoArgsConstructor
public class ApiRest<T>{

    private String  timestamp = DateTime.now().toString("yyyy-MM-dd HH:mm:ss");

    /**
      */
    private String msg;
    /**
     * 响应代码
     */
    private Integer code;

    /**
     * 请求或响应body
     */
    protected T data;



    /**
     * 构造函数
     * @param error
     */
    public ApiRest(ServiceException error){
        this.code = error.getCode();
        this.msg = error.getMsg();
    }

    /**
     * 构造函数
     * @param error
     */
    public ApiRest(ApiError error){
        this.code = error.getCode();
        this.msg = error.msg;
    }

}
